import utils.database
import models


def create_product_list_addapted(data: dict):
    return models.ProductList(
        id=str(data['_id']),
        name=data['name'],
        display_name=data["display_name"],
        image=data["image"]
    )


class ProductsListRepository:
    @staticmethod
    def compose(data: models.ProductList):
        connection = utils.database.connect()
        inserted = connection.product_list.insert_one({
            "name": data.name,
            "display_name": data.display_name,
            "image": data.image
        })
        item = connection.product_list.find_one({
            "_id": inserted.inserted_id
        })
        connection.client.close()
        return create_product_list_addapted(item)

    @staticmethod
    def find_all():
        connection = utils.database.connect()
        items = [create_product_list_addapted(
            item) for item in connection.product_list.find()]
        connection.client.close()
        return items
