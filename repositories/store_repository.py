import models
from utils import database
from bson import ObjectId


def create_store_addapted(data: dict):
    return models.Store(
        id=str(data["_id"]),
        name=data["name"],
        base_url=data["base_url"]
    )


class StoreRepository:
    @staticmethod
    def find_all():
        connection = database.connect()
        items = [create_store_addapted(item) for item in connection.store.find()]
        connection.client.close()
        return items

    @staticmethod
    def find_one(store_id: str):
        connection = database.connect()
        item = connection.store.find_one({"_id": ObjectId(store_id)})

        connection.client.close()
        return create_store_addapted(item)

    @staticmethod
    def compose(data: models.Store):
        connection = database.connect()

        inserted = connection.store.insert_one({
            "name": data.name,
            "base_url": data.base_url
        })

        item = connection.store.find_one({"_id": inserted.inserted_id})
        connection.client.close()
        return create_store_addapted(item)

    @staticmethod
    def find_one_by_name(name: str):
        connection = database.connect()

        item = connection.store.find_one({"name": name})

        connection.client.close()
        if item is None:
            return None
        return create_store_addapted(item)
