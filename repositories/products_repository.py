import models
import utils.database


def create_product_addapted(data: dict):
    return models.Product(
        id=str(data["_id"]),
        name=data["name"],
        display_name=data["display_name"],
        image=data["image"],
        store_id=data["store_id"],
        price=data["price"],
        original_url=data["original_url"]
    )


class ProductsRepository:
    @staticmethod
    def find_all():
        connection = utils.database.connect()
        items = [create_product_addapted(item)
                 for item in connection.products.find()]
        connection.client.close()
        return items

    @staticmethod
    def find_one(product_id: str):
        connection = utils.database.connect()
        item = connection.products.find_one({"_id": product_id})
        connection.client.close()
        if item is None:
            return item
        return create_product_addapted(item)

    @staticmethod
    def compose(data: models.Product):
        connection = utils.database.connect()
        inserted = connection.products.insert_one({
            "name": data.name,
            "display_name": data.display_name,
            "original_url": data.original_url,
            "image": data.image,
            "price": data.price,
            "store_id": data.store_id,
        })
        item = connection.products.find_one({"_id": inserted.inserted_id})
        connection.client.close()

        return create_product_addapted(item)

    @staticmethod
    def find_one_cheapest_by_name(product_name: str):
        connection = utils.database.connect()
        items = connection.products.find(
            {"name": product_name}).sort("price").limit(1)
        data = [create_product_addapted(item) for item in items]
        connection.client.close()
        return data[0]
