from fastapi import FastAPI, Request
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.staticfiles import StaticFiles

from repositories.products_list_repository import ProductsListRepository
from repositories.products_repository import ProductsRepository
from repositories.store_repository import StoreRepository


app = FastAPI()

app.mount("/static", StaticFiles(directory="static"), name="static")

templates = Jinja2Templates(directory="templates")


@app.get("/", response_class=HTMLResponse)
async def home(request: Request):
    products = ProductsListRepository.find_all()
    context = {
        "request": request,
        "products": products
    }

    return templates.TemplateResponse("home.html", context)


@app.get("/compare/{console_name}")
async def compare_console_prices(request: Request, console_name: str):
    if console_name is None:
        return RedirectResponse("/")
    product = ProductsRepository.find_one_cheapest_by_name(console_name)
    store = StoreRepository.find_one(product.store_id)

    context = {
        "request": request,
        "product": product,
        "store": store
    }
    template = templates.TemplateResponse(
        "compare_console_prices.html",
        context
    )
    return template
