import dataclasses


@dataclasses.dataclass()
class ProductList:
    id: str
    name: str
    display_name: str
    image: str


@dataclasses.dataclass()
class Store:
    id: str
    name: str
    base_url: str


@dataclasses.dataclass()
class Product:
    id: str
    name: str
    price: int
    display_name: str
    image: str
    original_url: str
    store_id: int
