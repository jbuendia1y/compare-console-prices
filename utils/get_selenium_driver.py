from selenium import webdriver


def get_selenium_driver():
    driver = webdriver.Chrome(executable_path="drivers/chromedriver")
    return driver
