import re


free_texts = [
    "GRATIS",
    "FREE",
]


def get_price(text: str):
    if text.upper().strip() in free_texts:
        return float(0.00)
    results = re.search(r'([0-9,]+(\.[0-9]{2})?)', text)
    return float(results.group(1).replace(",", "."))
