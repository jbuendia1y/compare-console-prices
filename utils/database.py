import pymongo
import os


def connect(db: str = None):
    """ connect function will connect with mongodb """
    # db param is the database name to connect by default is compares
    mongo_url = os.environ.get("MONGO_URL", None)
    if mongo_url:
        client = pymongo.MongoClient(mongo_url)
    else:
        client = pymongo.MongoClient()

    _db = client.main if mongo_url else client.compares

    if db is None:
        return _db

    return client[db]
