import scraper_to as scraper
import asyncio


async def main():
    await scraper.scrap()


if __name__ == '__main__':
    asyncio.run(main())
