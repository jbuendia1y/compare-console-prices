import asyncio

import bs4
import re

import constants
from utils import regexs
import models
import repositories.store_repository
import repositories.products_repository
import repositories.products_list_repository
from utils.get_selenium_driver import get_selenium_driver
import utils.regexs


class SagaFalabella:
    base_url = "https://www.falabella.com.pe/falabella-pe"
    store_id = "0"
    keys = [
        "Consola PlayStation 5".upper(),
        "Consola Ps5".upper(),
        "PS5".upper()
    ]

    def is_console_name(self, text: str):
        match_name = re.search(
            r"(CONSOLA PS5|CONSOLA PS 5|PS5|CONSOLA PLAYSTATION 5)", text)
        if match_name is None:
            return False
        match_plus = re.search(r"(\+| CON | Y | DUALSENSE )", text)
        if match_plus is None:
            return False

        return True

    async def scrap_item(self, item: bs4.element.PageElement):
        title_regex = re.compile("pod-subTitle")
        prices_regex = re.compile("prices")

        link_el = item.find(name="a")
        name_el = item.find(name="b", attrs={
            "class": title_regex
        })
        name: str = name_el.text

        if self.is_console_name(name.upper()) is False:
            return None

        image_el = item.find(name="img")
        prices_el = item.find(name="div", attrs={
            "class": prices_regex
        }).find_all("li")[0]

        if image_el is None:
            image = ""
        else:
            image = image_el.get("src", None)
            if image is None:
                image = image_el.get("data-src", "")

        original_url = link_el['href']

        data = models.Product(
            id="",
            name=constants.PS5_CONSOLE,
            display_name=name,
            image=image,
            price=regexs.get_price(prices_el.text),
            original_url=original_url,
            store_id=self.store_id
        )

        created = repositories.products_repository.ProductsRepository.compose(
            data)
        print(created)
        return created

    async def run(self):
        store_name = "SagaFalabella".upper()
        store = repositories.store_repository.StoreRepository.find_one_by_name(
            store_name)
        if store is None:
            store = repositories.store_repository.StoreRepository.compose(
                models.Store(id="", name=store_name, base_url=self.base_url)
            )
        self.store_id = store.id

        driver = get_selenium_driver()
        driver.get(
            "https://www.falabella.com.pe/falabella-pe/search?Ntt=consola+playstation+5")

        html_source = driver.page_source
        soup = bs4.BeautifulSoup(html_source, "lxml")

        driver.close()

        items = soup.find_all(name="div", attrs={
            "data-pod": "catalyst-pod"
        })

        await asyncio.gather(*[self.scrap_item(item) for item in items])


class Ripley:
    store_id = "0"
    base_url = "https://simple.ripley.com.pe"

    keys = [
        "PLAYSTATION 5",
        "CONSOLA PS5",
        "CONSOLA PLAYSTATION5"
    ]

    async def scarp_item(self, item: bs4.element.PageElement):
        title_regex = re.compile("catalog-product-details__name")
        link_el = item.find(name="a")
        image_el = item.find(name="img")

        title_el = item.find(name="div", attrs={
            "class": title_regex
        })
        prices_els = item.find(name="ul", attrs={
            "class": "catalog-prices__list"
        }).find_all(name="li")

        price = utils.regexs.get_price(prices_els[len(prices_els) - 1].text)

        image = image_el.get("src", None)
        if image is None:
            image = image_el.get("data-src", "")

        original_url = link_el['href']
        data = models.Product(
            id="0",
            name=constants.PS5_CONSOLE,
            price=price,
            display_name=title_el.text,
            image=image,
            store_id=self.store_id,
            original_url=original_url
        )

        product = repositories.products_repository.ProductsRepository.compose(
            data)

        return product

    async def run(self):
        store_name = "Ripley".upper()
        store = repositories.store_repository.StoreRepository.find_one_by_name(
            store_name)
        if store is None:
            store = repositories.store_repository.StoreRepository.compose(
                models.Store(id="", name=store_name, base_url=self.base_url)
            )
        self.store_id = store.id

        item_regex = re.compile("catalog-product-item")
        driver = get_selenium_driver()
        driver.get(
            "https://simple.ripley.com.pe/tecnologia/videojuegos/playstation-consolas?source=search&term=playstation%205")

        html_source = driver.page_source
        soup = bs4.BeautifulSoup(html_source, "lxml")

        driver.close()
        items = soup.find_all(name="div", attrs={
            "class": item_regex
        })

        await asyncio.gather(*[self.scarp_item(item) for item in items])


async def scrap():
    consoles = [models.ProductList(
        name=constants.PS5_CONSOLE,
        display_name="Playstation 5",
        image="https://res.cloudinary.com/dzur9okbf/image/upload/q_44/v1655067958/compare-console-prices/PS5_logo_ywykou.webp"
    )]
    repository = repositories.products_list_repository.ProductsListRepository
    exist = len(repository.find_all())
    if not exist:
        for console in consoles:
            repository.compose(console)

    await asyncio.gather(
        SagaFalabella().run(),
        Ripley().run()
    )
